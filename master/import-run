#!/bin/bash

set -e
set -u

BASE="/srv/snapshot.debian.org"
PATH="/usr/bin:/bin:$BASE/bin"
export TZ=UTC
umask 022

CONFIG="$BASE/etc/snapshot.conf"

if [ -z "${HOME:-}" ]; then
	HOME="$(getent passwd "`id -u`" | awk -F: '{print $6}')"
	export HOME
fi

LC_CTYPE=C.UTF-8
export LC_CTYPE

if [ "${2:-""}" = "-v" ] ; then
	verbose="--verbose"
else
	verbose=""
fi

lock=/var/run/reboot-lock
exec 203< "$lock"
if ! flock --shared 203; then
	echo >&2 "Warning: Cannot acquire reboot lock."
fi

archive="${1:-""}"

#IGNORES="--ignore /Archive-Update-in-Progress-stabile.debian.org --ignore /Archive-Update-Required-stabile.debian.org"
case "$archive" in
	"debian"|"debian-security"|"debian-volatile"|"debian-archive"|"debian-backports"|"debian-ports"|"debian-debug")
		force_slow_flag="$BASE/FORCE-SLOW-$archive"
		if [ -e "$force_slow_flag" ]; then
			quick=""
		else
			quick="--quick"
		fi
		snapshot import --config "$CONFIG" --archive "$archive" --path /srv/mirrors/"$archive" $quick $verbose
		rm -f "$force_slow_flag"

		ssh -T -i "$BASE"/.ssh/id-trigger lw01 < /dev/null > /dev/null 2>&1 &
		ssh -T -i "$BASE"/.ssh/id-trigger lw02 < /dev/null > /dev/null 2>&1 &
		ssh -T -i "$BASE"/.ssh/id-trigger lw03 < /dev/null > /dev/null 2>&1 &
		ssh -T -i "$BASE"/.ssh/id-trigger lw04 < /dev/null > /dev/null 2>&1 &
		ssh -T -i "$BASE"/.ssh/id-trigger lw09 < /dev/null > /dev/null 2>&1 &
		ssh -T -i "$BASE"/.ssh/id-trigger lw10 < /dev/null > /dev/null 2>&1 &

		"$BASE"/code/misc/dump-tools/add-new-dumps-to-git -c "$CONFIG" -b "$BASE"/dumps-git

		indexlock="$BASE/.snapshot-index.lock"
		touch "$indexlock"
		exec 204< "$indexlock"
		if ! flock --exclusive 204; then
			echo >&2 "Warning: Not running index as we don't have the lock."
		else
			snapshot index --config "$CONFIG" --quick $verbose
		fi

		ls -l "$BASE"/fuse/.ctrl-reload-database > /dev/null 2>&1 || true

		;;
	*)
		echo "No/Unknown archive given: $archive" >&2
		exit 1
esac

